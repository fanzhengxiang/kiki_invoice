package com.aires.kiki_invoice.entity;

import lombok.Data;

import java.util.List;

@Data
public class Invoice {
    private String filePath;
    private String invoiceCodeConfirm;
    private String invoiceNumConfirm;
    private String invoiceDate;
    private String checkCode;
    private String purchaserName;
    private String sellerName;
    private String purchaserRegisterNum;
    private String sellerRegisterNum;
    private List<Product> products;
    private String totalAmount;
    private String totalTax;
    private String amountInWords;
    private String amountInFiguers;
    private String noteDrawer;

}


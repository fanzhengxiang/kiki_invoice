package com.aires.kiki_invoice.entity;

import lombok.Data;

@Data
public class Product {
    private String commodityName;
    private String commodityType;
    private String commodityUnit;
    private String commodityNum;
    private String commodityPrice;
    private String commodityAmount;
    private String commodityTaxRate;
    private String commodityTax;
}

package com.aires.kiki_invoice.controller;

import cn.hutool.core.io.FileUtil;
import com.aires.kiki_invoice.entity.Invoice;
import com.aires.kiki_invoice.service.InvoiceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    @Resource
    private InvoiceService invoiceService;

    @PostMapping("/upload")
    public void parseInvoice(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws IOException {

        try {
            InputStream inputStream = file.getInputStream();
            File tempFile = File.createTempFile(String.valueOf(System.currentTimeMillis()), ".doc");
            FileUtil.writeFromStream(inputStream, tempFile);
            FileInputStream fis = new FileInputStream(tempFile);
            // 获取所有图片


            XWPFDocument document = new XWPFDocument(fis);
            List<XWPFPictureData> pictures = new ArrayList<>();
            List<XWPFParagraph> paragraphs = document.getParagraphs();
            for (XWPFParagraph paragraph : paragraphs) {
                for (XWPFRun run : paragraph.getRuns()) {
                    List<XWPFPicture> embeddedPictures = run.getEmbeddedPictures();
                    for (XWPFPicture embeddedPicture : embeddedPictures) {
                        pictures.add(embeddedPicture.getPictureData());
                    }
                }

            }


//            List<XWPFPictureData> pictures = document.getAllPictures();
            for (int i = 0; i < pictures.size(); i++) {
                XWPFPictureData picture = pictures.get(i);
                // 获取图片类型
                String contentType = picture.getPackagePart().getContentType();
                // 获取图片数据
                byte[] imageData = picture.getData();
                // 保存图片到本地文件
                invoiceService.saveImage(contentType, imageData, i);
            }

            document.close();
            fis.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String property = System.getProperty("user.dir");
        File currentDir = new File(property);
        File[] files = currentDir.listFiles();
        List<File> allInvoicePics = Arrays.asList(files).stream().filter(e -> e.isFile() && !e.getName().contains("xml") && e.getName().contains("image")).collect(Collectors.toList());
        CopyOnWriteArrayList<Invoice> list = new CopyOnWriteArrayList<>();
        allInvoicePics.forEach(e -> {
            String absolutePath = e.getAbsolutePath();
            Invoice invoices = null;
            try {
                invoices = invoiceService.parserInvoice(absolutePath);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            if (Objects.nonNull(invoices)) {
                log.info("absolutePath是发票:{}", absolutePath);
                list.add(invoices);
            } else {
                log.info("absolutePath不是发票:{}", absolutePath);
            }
        });
        // 处理最后一张统计
        BigDecimal totalAmount = list.stream().map(e -> {
            try {
                if (Objects.nonNull(e.getTotalAmount())){

                return new BigDecimal(e.getTotalAmount());
                }else {
                    return BigDecimal.ZERO;
                }
            } catch (Exception ex) {
                return BigDecimal.ZERO;
            }
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalTax = list.stream().map(e->{
            // 处理免税的情况 税额是这种格式 ***
            try {
                String tax = e.getTotalTax();
                if (tax.contains("*")){
                    return BigDecimal.ZERO;
                }else {
                    return new BigDecimal(tax);
                }
            } catch (Exception ex) {
                return BigDecimal.ZERO;
            }
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalAmountInFiguers = list.stream().map(e -> {
            try {
                return new BigDecimal(e.getAmountInFiguers());
            } catch (Exception ex) {
                return BigDecimal.ZERO;
            }
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
        // 开始导出excel
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Entities");

            // 创建表头
            Row headerRow = sheet.createRow(0);
            String[] headers = {"发票图片", "发票代码","发票号码", "开票日期", "校验码","购买方-名称", "购买方-社会代码", "销售方-名称", "销售方-社会代码", "合计金额", "合计税额", "统计大写", "小计", "开票人"};
            for (int i = 0; i < headers.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(headers[i]);
            }

            // 填充数据
            int rowNum = 1;
            for (int i = 0; i <= list.size(); i++) {
                if (i < list.size()) {
                    Invoice entity = list.get(i);
                    Row row = sheet.createRow(rowNum++);
                    insertImageToCell(entity.getFilePath(), row.createCell(0), workbook);

//                    row.createCell(0).setCellValue(entity.getFilePath());
                    row.createCell(1).setCellValue(entity.getInvoiceCodeConfirm());
                    row.createCell(2).setCellValue(entity.getInvoiceNumConfirm());
                    row.createCell(3).setCellValue(entity.getInvoiceDate());
                    row.createCell(4).setCellValue(entity.getCheckCode());
                    row.createCell(5).setCellValue(entity.getPurchaserName());
                    row.createCell(6).setCellValue(entity.getPurchaserRegisterNum());
                    row.createCell(7).setCellValue(entity.getSellerName());
                    row.createCell(8).setCellValue(entity.getSellerRegisterNum());
                    row.createCell(9).setCellValue(entity.getTotalAmount());
                    row.createCell(10).setCellValue(entity.getTotalTax());
                    row.createCell(11).setCellValue(entity.getAmountInWords());
                    row.createCell(12).setCellValue(entity.getAmountInFiguers());
                    row.createCell(13).setCellValue(entity.getNoteDrawer());
                } else {
                    Row row = sheet.createRow(rowNum++);
                    row.createCell(0).setCellValue("统计汇总");
                    row.createCell(9).setCellValue(totalAmount.toString());
                    row.createCell(10).setCellValue(totalTax.toString());
                    row.createCell(12).setCellValue(totalAmountInFiguers.toString());
                }

            }
            // 写入workbook后删除文件
            String filePath = list.get(0).getFilePath();
            String pathSeparator = File.separator;
            log.info("当前操作系统的文件分隔符是:"+pathSeparator );
            String rootDir = filePath.substring(0, filePath.lastIndexOf(pathSeparator));
            File file1 = new File(rootDir);
            if (file1.exists()) {
                File[] f = file1.listFiles();
                Arrays.asList(f).parallelStream().forEach(e -> {
                    if ((e.getName().contains("png") || e.getName().contains("jpg") || e.getName().contains("jpeg")) && e.getName().contains("image")  ) {
                        e.delete();
                        System.out.println("delete..." + e.getAbsolutePath());
                    }
                });
            }
            // 获取 Servlet 输出流
            ServletOutputStream outputStream = response.getOutputStream();
            // 将 Excel 工作簿写入输出流
            workbook.write(outputStream);
            // 关闭输出流
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void insertImageToCell(String imagePath, Cell cell, Workbook workbook) throws IOException {
        if (imagePath == null || imagePath.isEmpty()) {
            return;
        }

        BufferedImage bufferedImage = ImageIO.read(new File(imagePath));
        ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", byteArrayOut);
        int pictureIdx = workbook.addPicture(byteArrayOut.toByteArray(), Workbook.PICTURE_TYPE_JPEG);
        byteArrayOut.close();

        CreationHelper helper = workbook.getCreationHelper();
        Drawing<?> drawing = cell.getSheet().createDrawingPatriarch();
        ClientAnchor anchor = helper.createClientAnchor();

        anchor.setCol1(cell.getColumnIndex());
        anchor.setRow1(cell.getRowIndex());
        anchor.setCol2(cell.getColumnIndex() + 1);
        anchor.setRow2(cell.getRowIndex() + 1);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);

        Picture pict = drawing.createPicture(anchor, pictureIdx);
        pict.resize(0.8);
    }
}

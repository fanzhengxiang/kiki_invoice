package com.aires.kiki_invoice.service;

import com.aires.kiki_invoice.entity.Invoice;
import com.aires.kiki_invoice.entity.Product;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson2.JSONWriter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Value("${ak}")
    private String apiKey;
    @Value("${sk}")
    private String secretKey;

    @Override
    public Invoice parserInvoice(String path) throws IOException {
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        // image 可以通过 getFileContentAsBase64("C:\fakepath\image_1.png") 方法获取,如果Content-Type是application/x-www-form-urlencoded时,第二个参数传true
        RequestBody body = RequestBody.create(mediaType, getFileContentAsBase64(path, true));
        Request request = new Request.Builder().url("https://aip.baidubce.com/rest/2.0/ocr/v1/vat_invoice?access_token=" + getAccessToken()).method("POST", body).addHeader("Content-Type", "application/x-www-form-urlencoded").addHeader("Accept", "application/json").build();
        OkHttpClient HTTP_CLIENT = new OkHttpClient().newBuilder().build();
        Response response = HTTP_CLIENT.newCall(request).execute();
        String string = response.body().string();
        com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(string);
        Integer errorCode = jsonObject.getInteger("error_code");
        if (Objects.nonNull(errorCode)) {
            return null;
        }
        com.alibaba.fastjson.JSONObject wordsResult = jsonObject.getJSONObject("words_result");
        Invoice invoice = new Invoice();
        String invoiceCodeConfirm = wordsResult.getString("InvoiceCodeConfirm");
        invoice.setInvoiceCodeConfirm(invoiceCodeConfirm);
        invoice.setFilePath(path);
        String invoiceNumConfirm = wordsResult.getString("InvoiceNumConfirm");
        invoice.setInvoiceNumConfirm(invoiceNumConfirm);
        String InvoiceDate = wordsResult.getString("InvoiceDate");
        invoice.setInvoiceDate(InvoiceDate);
        String checkCode = wordsResult.getString("CheckCode");
        invoice.setCheckCode(checkCode);
        String PurchaserName = wordsResult.getString("PurchaserName");
        invoice.setPurchaserName(PurchaserName);
        String SellerName = wordsResult.getString("SellerName");
        invoice.setSellerName(SellerName);
        String PurchaserRegisterNum = wordsResult.getString("PurchaserRegisterNum");
        invoice.setPurchaserRegisterNum(PurchaserRegisterNum);
        String SellerRegisterNum = wordsResult.getString("SellerRegisterNum");
        invoice.setSellerRegisterNum(SellerRegisterNum);
        JSONArray commodityName = wordsResult.getJSONArray("CommodityName");
        JSONArray CommodityType = wordsResult.getJSONArray("CommodityType");
        JSONArray CommodityUnit = wordsResult.getJSONArray("CommodityUnit");
        JSONArray CommodityNum = wordsResult.getJSONArray("CommodityNum");
        JSONArray CommodityPrice = wordsResult.getJSONArray("CommodityPrice");
        JSONArray CommodityAmount = wordsResult.getJSONArray("CommodityAmount");
        JSONArray CommodityTaxRate = wordsResult.getJSONArray("CommodityTaxRate");
        JSONArray CommodityTax = wordsResult.getJSONArray("CommodityTax");
        List<Product> list = new ArrayList<>();
        for (int i = 0; i < commodityName.size(); i++) {
            Product product = new Product();
            com.alibaba.fastjson.JSONObject o1 = (com.alibaba.fastjson.JSONObject) commodityName.get(i);

            com.alibaba.fastjson.JSONObject o2 = new com.alibaba.fastjson.JSONObject();
            if (!CollectionUtils.isEmpty(CommodityType)) {
                o2 = (com.alibaba.fastjson.JSONObject) CommodityType.get(i);
            }
            com.alibaba.fastjson.JSONObject o3 = new com.alibaba.fastjson.JSONObject();
            if (!CollectionUtils.isEmpty(CommodityUnit)) {

                o3 = (com.alibaba.fastjson.JSONObject) CommodityUnit.get(i);
            }
            com.alibaba.fastjson.JSONObject o4 = new com.alibaba.fastjson.JSONObject();
            if (!CollectionUtils.isEmpty(CommodityNum)) {
                if (CommodityNum.size()>i){

                o4 = (com.alibaba.fastjson.JSONObject) CommodityNum.get(i);
                }
            }
            com.alibaba.fastjson.JSONObject o5 = new com.alibaba.fastjson.JSONObject();
            if (!CollectionUtils.isEmpty(CommodityPrice)) {
                if (CommodityPrice.size()>i){

                o5 = (com.alibaba.fastjson.JSONObject) CommodityPrice.get(i);
                }
            }
            com.alibaba.fastjson.JSONObject o6 = new com.alibaba.fastjson.JSONObject();
            if (!CollectionUtils.isEmpty(CommodityAmount)) {

                o6 = (com.alibaba.fastjson.JSONObject) CommodityAmount.get(i);
            }
            com.alibaba.fastjson.JSONObject o7 = new com.alibaba.fastjson.JSONObject();
            if (!CollectionUtils.isEmpty(CommodityTaxRate)) {
                o7 = (com.alibaba.fastjson.JSONObject) CommodityTaxRate.get(i);
            }
            com.alibaba.fastjson.JSONObject o8 = new com.alibaba.fastjson.JSONObject();
            if (!CollectionUtils.isEmpty(CommodityTax)) {
                o8 = (com.alibaba.fastjson.JSONObject) CommodityTax.get(i);
            }
            product.setCommodityName(o1.getString("word"));
            product.setCommodityType(o2.getString("word"));
            product.setCommodityUnit(o3.getString("word"));
            product.setCommodityNum(o4.getString("word"));
            product.setCommodityPrice(o5.getString("word"));
            product.setCommodityAmount(o6.getString("word"));
            product.setCommodityTax(o7.getString("word"));
            product.setCommodityTax(o8.getString("word"));
            list.add(product);
        }
        invoice.setProducts(list);
        String TotalAmount = wordsResult.getString("TotalAmount");
        invoice.setTotalAmount(TotalAmount);
        String TotalTax = wordsResult.getString("TotalTax");
        invoice.setTotalTax(TotalTax);
        String AmountInWords = wordsResult.getString("AmountInWords");
        invoice.setAmountInWords(AmountInWords);
        String AmountInFiguers = wordsResult.getString("AmountInFiguers");
        invoice.setAmountInFiguers(AmountInFiguers);
        String NoteDrawer = wordsResult.getString("NoteDrawer");
        invoice.setNoteDrawer(NoteDrawer);
        System.out.println(com.alibaba.fastjson2.JSONObject.toJSONString(invoice, JSONWriter.Feature.PrettyFormat));
        return invoice;
    }

    // 保存图片到本地文件
    @Override
    public void saveImage(String contentType, byte[] imageData, int index) {
        try {
            // 根据图片类型确定文件扩展名
            String fileExtension = "";
            if (contentType.equals("image/jpeg")) {
                fileExtension = "jpg";
            } else if (contentType.equals("image/png")) {
                fileExtension = "png";
            }

            // 保存图片到本地文件
            FileOutputStream fos = new FileOutputStream("image_" + index + "_" + new DateTime().toString("yyyy_MM_dd_HH_mm_sss") + "." + fileExtension);
            fos.write(imageData);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getFileContentAsBase64(String path, boolean urlEncode) throws IOException {
        byte[] b = Files.readAllBytes(Paths.get(path));
        String base64 = Base64.getEncoder().encodeToString(b);
        if (urlEncode) {
            base64 = URLEncoder.encode(base64, String.valueOf(StandardCharsets.UTF_8));
        }
        return "image=" + base64;
    }


    /**
     * 从用户的AK，SK生成鉴权签名（Access Token）
     *
     * @return 鉴权签名（Access Token）
     * @throws IOException IO异常
     */
    private String getAccessToken() throws IOException {
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials&client_id=" + apiKey + "&client_secret=" + secretKey);
        Request request = new Request.Builder().url("https://aip.baidubce.com/oauth/2.0/token").method("POST", body).addHeader("Content-Type", "application/x-www-form-urlencoded").build();
        OkHttpClient HTTP_CLIENT = new OkHttpClient().newBuilder().build();
        Response response = HTTP_CLIENT.newCall(request).execute();
        return new JSONObject(response.body().string()).getString("access_token");
    }
}

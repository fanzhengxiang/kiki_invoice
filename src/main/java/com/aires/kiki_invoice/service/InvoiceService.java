package com.aires.kiki_invoice.service;

import com.aires.kiki_invoice.entity.Invoice;

import java.io.IOException;

public interface InvoiceService {
    

    Invoice parserInvoice(String property) throws IOException;

    void saveImage(String contentType, byte[] imageData, int index);
}

package com.aires.kiki_invoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KikiInvoiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(KikiInvoiceApplication.class, args);
    }

}
